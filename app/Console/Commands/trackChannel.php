<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Redis;
use App\Models\Bind;

class trackChannel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:channel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tracks all mentioned channels, looking for new post.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        foreach( Bind::where('edge', 0)->get() as $item ){
            
            echo $item->channel.PHP_EOL;

            if(! $res = @file_get_contents('http://ceiz.ezze.li:6583/?do=getMessage&onlyMessage=0&channel=' . $item->channel . '&count=2') ){
                echo __LINE__.': cant get true result from `madeline`'.PHP_EOL;

            } else {
                
                $res = json_decode($res, true);
                if( $res['status'] != 'OK' ){
                    echo __LINE__.': cant get JSON result from `madeline`'.PHP_EOL;

                } else {
                    
                    // get the new posts, if any
                    $new_posts = [];
                    foreach( $post = $res['res']['messages'] as $post ){
                        $batch = md5( $post['peer_id'] . $post['date'] . $post['message'] );
                        if( $item->batch == $batch )
                            break;
                        $new_posts[] = $post;
                    }

                    // we have new posts;
                    if( sizeof($new_posts) ){
                        
                        echo __LINE__.': we have new posts'.PHP_EOL;

                        // update the md5
                        $post = $new_posts[0];
                        $item->batch = md5( $post['peer_id'] . $post['date'] . $post['message'] );
                        $item->save();

                        // emit new records
                        foreach( array_reverse($new_posts) as $post ){
                            if( $post['message'] = trim($post['message'], "\r\n\t ") ){
                                Redis::publish('telegram-channel-tracker:new-post', json_encode(['channel'=>$item->channel, 'message'=>$post['message']]));
                                sleep(1);
                            }
                        }

                    } else {
                        echo "nothing".PHP_EOL;
                    }
                }
            }

        }
    }
}
