<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Redis;
use App\Models\Bind;

class publishListener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:listener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Binded to find new redis publish requests.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        
        echo 'Start listening for redis publish request ..' . PHP_EOL;

        # https://laravel.com/docs/11.x/redis
        // Redis::psubscribe(['*'], function (string $message, string $channel) {
        //     echo $channel ." -> ". $message . PHP_EOL;
        //     // ... telegram-channel-tracker:bind-request
        //     // add to database this record, 
        // });

        Redis::subscribe(['telegram-channel-tracker:bind-request'], function ($message) {
            
            $req = json_decode($message, true);
            
            if(! $res = @file_get_contents('http://ceiz.ezze.li:6583/?do=getMessage&onlyMessage=0&channel=' . $req['channel'] . '&count=1') ){
                //
                echo __LINE__.PHP_EOL;

            } else {

                $res = json_decode($res, true);

                if( $res['status'] != 'OK' ){
                    // ..
                    echo __LINE__.PHP_EOL;
                
                } else {

                    $post = $res['res']['messages'][0];
                    $batch = md5( $post['peer_id'] . $post['date'] . $post['message'] );

                    echo $batch.PHP_EOL;

                    if( Bind::where('channel', $req['channel'])->count() ){
                        echo $req['channel']. ' => UPDATE'.PHP_EOL;
                        Bind::where('channel', $req['channel'])->update([ 'edge' => 0 ]);

                    } else {
                        echo $req['channel']. ' => CREATE'.PHP_EOL;
                        Bind::create([
                            'channel' => $req['channel'],
                            'batch' => $batch,
                        ]);
                    }

                }

            }

        });


    }
}