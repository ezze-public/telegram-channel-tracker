<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Redis;
use App\Models\Bind;

class bindAnnounce extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bind:announce';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ask other agents to send bind:request in case of need.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        sleep(50); // prevent overwrapping
        Bind::increment('edge');
        Bind::where('edge', 2)->delete();
        Redis::publish('telegram-channel-tracker:bind-announce', '{}');
    }
}
